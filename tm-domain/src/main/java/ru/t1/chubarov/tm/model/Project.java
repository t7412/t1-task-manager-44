package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractModel {

//    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    //    @NotNull
    @Nullable
    @Column(nullable = false, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Project(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    @Id
    @NotNull
    @Column(nullable = false, name = "id")
    private String id = UUID.randomUUID().toString();

}
