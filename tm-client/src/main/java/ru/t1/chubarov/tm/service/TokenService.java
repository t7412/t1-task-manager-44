package ru.t1.chubarov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.service.ITokenService;

@Getter
@Setter
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
